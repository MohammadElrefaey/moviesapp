//
//  HomeVM.swift
//  MoviesApp
//
//  Created by Maher on 9/8/21.
//

import Foundation
import UIKit

class HomeViewModel {
    // MARK:- Private Properties
    var categories = [Category.topRated, Category.popular, Category.comingSoon, Category.nowPlaying]
    var cellCategory: Box<Category?> = Box(nil)
    var goToVC: Box<UIViewController?> = Box(nil)
    var reloadData: Box<Void?> = Box(nil)
    var fetchedMovies : Box<[Results]> = Box([])
    var categoryTitle =  Box("")

    
    func ApiRequest(of category: String, completion: @escaping ([Results]) -> ())  {
        APIService.shared.getData(path: category) { (response: ResponseModel?, error) in
            if let error = error {
                print(error)
            }
            else {
                guard let movies = response?.results else { return }
                completion(movies)
            }
        }
    }
    
    func getMovies(cellCategory: Category) {
        switch cellCategory {
        case .topRated:
            categoryTitle.value = "Top Rated"
            ApiRequest(of: "top_rated", completion: { (movies) in
                self.fetchedMovies.value = movies
            })
            
        case .popular:
            categoryTitle.value = "Popular"
            ApiRequest(of: "popular", completion: { (movies) in
                self.fetchedMovies.value = movies
            })
            
        case .comingSoon:
            categoryTitle.value = "Coming Soon"
            ApiRequest(of: "upcoming", completion: { (movies) in
                self.fetchedMovies.value = movies
            })

        case .nowPlaying:
            categoryTitle.value = "Now Playing"
             ApiRequest(of: "now_playing", completion: { (movies) in
                self.fetchedMovies.value = movies
            })

        }
    }

    
    
    func goToDetailsVC(movie: Results) {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailesVC = Storyboard.instantiateViewController(withIdentifier: "DetailesVC") as! DetailsVC
        detailesVC.movieId = movie.id
        goToVC.value = detailesVC
     }
    
    private func goToCategoryVC(category: Category, movies: [Results]) {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let categoryVC = Storyboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
        categoryVC.viewModel.category = Box(category)
        categoryVC.viewModel.moviesArray = Box(movies)
        goToVC.value = categoryVC
     }

    
}

extension HomeViewModel: cellProtocol {
    func showAllBtnTapped(category: Category, movies: [Results]) {
        goToCategoryVC(category: category, movies: movies)
    }
        
    func reloadTable() {
        reloadData.value = nil
    }
    
    func movieTapped(movie: Results) {
        goToDetailsVC(movie: movie)
    }
        
}
