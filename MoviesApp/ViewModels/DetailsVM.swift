//
//  DetailsViewModel.swift
//  MoviesApp
//
//  Created by Maher on 9/12/21.
//

import Foundation



class DetailsVM {
    
    var movie: Box<Movie?> = Box(nil)
    var trailerVideo: Box<VideoModel?> = Box(nil)
    var review: Box<ReviewModel?> = Box(nil)
    var cast: Box<[Cast]> = Box([])


    func getData<T: Decodable>(movieID: String, completion: @escaping (T) -> ()) {
        APIService.shared.getData(path: movieID) { (data: T?, error) in
            if let error = error {
                print(error)
            }
            else {
                guard let data = data else { return }
                completion(data)
            }
        }
    }
    
    func getData(movieId: Int) {
        getData(movieID: String(movieId)) { (fetchedMovie: Movie) in
            self.movie.value = fetchedMovie
     }
     
        getData(movieID: "\(String(movieId))/videos") { (video: VideoModel) in
        self.trailerVideo.value = video
     }
     
        getData(movieID: "\(String(movieId))/reviews") { (review: ReviewModel) in
        self.review.value = review
     }
     
        getData(movieID: "\(String(movieId))/credits") { (credits: CreditModel) in
         guard let cast = credits.cast else { return}
        self.cast.value = cast
     }
    }

    
    
}
