//
//  CategoryVM.swift
//  MoviesApp
//
//  Created by Maher on 11/1/21.
//

import Foundation
import UIKit

class CategoryViewModel {
    
    var moviesArray: Box<[Results]> = Box([])
    var category: Box<Category?> = Box(nil)
    var gotoVC: Box<UIViewController?> = Box(nil)
    var title = Box("")
    
    
     func goToDetailsVC(movie: Results) {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailesVC = Storyboard.instantiateViewController(withIdentifier: "DetailesVC") as! DetailsVC
        detailesVC.movieId = movie.id
        gotoVC.value = detailesVC
     }
    
     func titleSetup() {
        switch category.value {
        case .topRated:
            title.value = "Top Rated Movies"
        
        case .popular:
            title.value = "Popular Movies"
            
        case .comingSoon:
            title.value = "Coming Soon Movies"
        
        case .nowPlaying:
            title.value = "Now Playing Movies"

        default:
            title.value = ""
        }

    }


}

