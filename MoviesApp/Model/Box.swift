//
//  Box.swift
//  MoviesApp
//
//  Created by Maher on 10/25/21.
//

import Foundation

class Box<T> {
  typealias Listner = (T) -> Void
  var listener: Listner?
  
  var value: T {
      didSet {
        listener?(value)
      }
    }
  
  init(_ value: T) {
    self.value = value
  }
  
  func bind(listener: Listner?) {
    self.listener = listener
    listener?(value)
  }
  
}
