//
//  TableViewCellModel.swift
//  CreativeMinds-Task
//
//  Created by Mohamed on 5/27/21.
//

import Foundation

enum Category: String {
    case topRated
    case popular
    case comingSoon
    case nowPlaying
}
