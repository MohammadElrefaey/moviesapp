//
//  CategoryVC.swift
//  MoviesApp
//
//  Created by Maher on 9/12/21.
//
import UIKit

class CategoryVC: UIViewController {
    //MARK:- outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- properties
    var viewModel = CategoryViewModel()
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSetub()
        bind()
        
    }
    
    //Mark:- Binding
    func bind() {
        viewModel.titleSetup()
        viewModel.title.bind { [weak self] (title) in
            guard let self = self else {return}
            self.title = title
        }

        viewModel.gotoVC.bind { [weak self] (vc) in
            guard let self = self else {return}
            guard let vc = vc else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }

}


//MARK:- CollectionView Delegate & DataSource
extension CategoryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.moviesArray.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell {
            cell.configure(path: viewModel.moviesArray.value[indexPath.row].poster_path)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = view.frame.width
        let cellWidth = (width - 30) / 2
        let cellHeght = cellWidth * 1.5
        
        return CGSize(width: cellWidth, height: cellHeght)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.goToDetailsVC(movie: viewModel.moviesArray.value[indexPath.row])
    }
}

//MARK:- Private Methods
extension CategoryVC {
    private func collectionSetub() {
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
         collectionView.reloadData()
     }


    
}
