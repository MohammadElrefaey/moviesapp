//
//  TableViewCell.swift
//  MoviesApp
//
//  Created by Maher on 9/8/21.
//

import UIKit
// protcol to delegate the tableView cell functions to HomeVC
protocol cellProtocol: class {
    func reloadTable()
    func movieTapped(movie: Results)
    func showAllBtnTapped(category: Category, movies: [Results])
}

class TableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionHeigh: NSLayoutConstraint!
    @IBOutlet weak var collapseBtn: UIButton!
    
    //MARK:- Properties
    private var viewModel = HomeViewModel()
    weak var delegate: cellProtocol?
    
    var isCollapsed = false {
        didSet {
            //Change image
            collapseBtn.setImage( isCollapsed ? UIImage(systemName: "chevron.up") : UIImage(systemName: "chevron.down"), for: .normal)
            
            //Change collection height
            collectionHeigh.constant = isCollapsed ? 0 : 180
            delegate?.reloadTable()
            UIView.animate(withDuration: 5) {
                self.collectionView.layoutIfNeeded()
            }
        }
    }
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionSetub()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(category: Category) {
        viewModel.cellCategory = Box(category)
        
        
        self.viewModel.categoryTitle.bind { (title) in
            self.categoryLbl.text = title
        }
        
        self.viewModel.fetchedMovies.bind { (movies) in
            self.collectionView.reloadData()
        }

        self.viewModel.getMovies(cellCategory: category)
    }

    //MARK:- Actions
    @IBAction func collapseBtnTapped(_ sender: UIButton) {
        isCollapsed = !isCollapsed
    }

    @IBAction func showAllBtn(_ sender: UIButton) {
        viewModel.cellCategory.bind { (category) in
            guard let category = category else {return}
            self.delegate?.showAllBtnTapped(category: category, movies: self.viewModel.fetchedMovies.value)
        }
    }
}

//MARK:- CollectionView Delegate & DataSource
extension TableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.fetchedMovies.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell {
            cell.configure(path: viewModel.fetchedMovies.value[indexPath.row].poster_path)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 120, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.movieTapped(movie: viewModel.fetchedMovies.value[indexPath.row])
    }
}

//MARK:- Private Methods
extension TableViewCell {
    
   private func collectionSetub() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
    }
    
}
