//
//  CollectionViewCell.swift
//  MoviesApp
//
//  Created by Maher on 9/8/21.
//

import UIKit
import Kingfisher

class CollectionViewCell: UICollectionViewCell {
    
//MARK:- properties
    // baser URL for movie poster
    let baseUrl = "https://image.tmdb.org/t/p/w154/"
    
//MARK:- outlets
    @IBOutlet weak var movieImg: RoundedImageViews!

    //MARK:- life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(path: String?) {
        guard let path = path else {return}
        let url = URL(string: "\(baseUrl)\(path)")
        movieImg.kf.setImage(with: url)
    }
}
