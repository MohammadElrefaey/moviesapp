//
//  HomeVC.swift
//  MoviesApp
//
//  Created by Maher on 9/8/21.
//

import UIKit

class HomeVC: UIViewController {
    
    //MARK:- Properties
    
    private var viewModel = HomeViewModel()
    
    //MARK:- outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableSetup()
        binding()
    }
   
    // MARK: - Binding
    func binding() {
        viewModel.goToVC.bind { (vc) in
            guard let vc = vc else { return }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        viewModel.reloadData.bind { (_) in
            self.tableView.reloadData()
        }
    }

}
//MARK:- UITableViewDataSource, UITableViewDelegate
extension HomeVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.categories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        
        
        cell.configure(category: viewModel.categories[indexPath.row])
        // take delegate from table cell
        cell.delegate = viewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}


//MARK:- Private Methods
extension HomeVC {
    private func tableSetup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
    }
    
    func reload() {
        tableView.reloadData()
    }
    

}
