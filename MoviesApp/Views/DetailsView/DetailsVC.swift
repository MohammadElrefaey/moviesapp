//
//  DetailsVC.swift
//  MoviesApp
//
//  Created by Maher on 9/12/21.
//

import UIKit
import Kingfisher

class DetailsVC: UIViewController {
    //MARK:- outlets
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var posterImg: RoundedImageViews!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var genersLbl: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var overviewLbl: UILabel!
    @IBOutlet weak var castCollectionView: UICollectionView!
    @IBOutlet weak var overViewLblHeight: NSLayoutConstraint!
    
    //MARK:- properties
    var viewModel = DetailsVM()
    var linesNumber = 0
    var movieId: Int!
    var geners = [String]()
    var trialerId: String!
    var review: String!
    
    //MARK:- life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSetub()
        viewModel.getData(movieId: movieId)
        binding()
    }
    
    //MARK:- Binding
    func binding() {
        viewModel.movie.bind { (movie) in
            guard let movie = movie else {return}
            self.titleLbl.text = movie.title
            self.rateLbl.text = "\(String(movie.vote_average ?? 0)) (\(String(movie.vote_count ?? 0)) Reviews)"
            self.releaseDateLbl.text = "\(movie.release_date ?? "") Released"
            self.durationLbl.text = "\(String(movie.runtime ?? 0)) mins"
            self.overviewLbl.text = movie.overview
            self.review = movie.overview
            self.setOverviewLabel()

            // setup generes label
            guard let generes = movie.genres else {return}
            
            for gener in generes {
                self.geners.append(gener.name!)
            }
            self.genersLbl.text = self.geners.joined(separator:",")

            // get movie poster
            guard let posterPath = movie.poster_path else {return}
            let url = URL(string: "https://image.tmdb.org/t/p/w185/" + posterPath)
            self.posterImg.kf.setImage(with: url)

            // get movie cover
            guard let coverPath = movie.backdrop_path else {return}
            let urll = URL(string: "https://image.tmdb.org/t/p/w185/" + coverPath)
            self.coverImg.kf.setImage(with: urll)

        }
        
//        viewModel.trailerVideo.bind { (video) in
//            guard let video = video?.results, let trialerId = video[0].id else {return}
//            self.trialerId = trialerId
//        }
        

        viewModel.cast.bind { (cast) in
         //   self.cast = cast
            self.castCollectionView.reloadData()

        }


    }


    //MARK:- Actions
    @IBAction func playTrailerBtnTapped(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.youtube.com/watch?v=\(String(trialerId))") {
            UIApplication.shared.open(url)
        }
    }
    
}
//MARK:- CollectionView Delegate & DataSource
extension DetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cast.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell {
            cell.configure(path: viewModel.cast.value[indexPath.row].profile_path)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 120, height: 180)
    }
        
}


//MARK:- Private Methods
extension DetailsVC {
    
    private func setOverviewLabel() {
        if overviewLbl.maxNumberOfLines > 3 {
            linesNumber = overviewLbl.maxNumberOfLines + 1
            DispatchQueue.main.async {
                self.overviewLbl.addTrailing(with: "... ", moreText: "Read More", moreTextFont: UIFont.systemFont(ofSize: 14), moreTextColor: .black)
                
                guard let text = self.overviewLbl.text else { return }
                let underlineAttriString = NSMutableAttributedString(string: text)
                let range1 = (text as NSString).range(of: "Read More")
                underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
                underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(.blue), range: range1)

                self.overviewLbl.attributedText = underlineAttriString
                self.overviewLbl.isUserInteractionEnabled = true

                self.overviewLbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(self.readMore(_:))))

            }
        }
        

    }
    
    @objc func readMore(_ sender: UITapGestureRecognizer) {
        overviewLbl.numberOfLines = linesNumber
        overviewLbl.text = review
        overViewLblHeight.constant = CGFloat(Double(linesNumber)*20)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }

    }

    
    private func collectionSetub() {
        castCollectionView.delegate = self
        castCollectionView.dataSource = self
        castCollectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        castCollectionView.reloadData()
     }



}
