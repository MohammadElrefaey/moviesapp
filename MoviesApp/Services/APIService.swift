//
//  APIServices.swift
//  CreativeMinds-Task
//
//  Created by Mohamed on 5/26/21.
//

import Foundation
import Alamofire

class APIService {
    static let shared = APIService()

    // Generic newtork function using Alamofire and fixed api key parametere
    
    func getData<T: Decodable>(path: String, completion: @escaping (T?, Error?) -> ()) {
        let baseUrl = "https://api.themoviedb.org/3/movie/"
        let url = "\(baseUrl)\(path)"
        let apiKey = ["api_key": "72e6acb880560e040496e75062622dc2"]
        
        AF.request(url, method: HTTPMethod.get, parameters: apiKey, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
            case .success(_):
                do {
                    guard let data = response.data else { return }
                    let results = try JSONDecoder().decode(T.self, from: data)
                  completion(results, nil)
                    
                } catch let error {
                    print (error)
                }
                
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
//    func getData<T: Decodable>(path: String, completion: @escaping (T) -> ()) {
//        request(path: path) { (data: T?, error) in
//            if let error = error {
//                print(error)
//                print("aho\(error)")
//
//            }
//            else {
//                guard let data = data else { return }
//                print("aho\(data)")
//                completion(data)
//            }
//        }
//    }

}

